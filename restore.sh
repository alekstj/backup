#!/bin/bash
#

APP="$PWD/bin/unsquashfs-`uname -m`"
COMMAND="$APP -f -d /mnt/gentoo $1"

if [ "$1" = "" ]; then
	printf " # ERROR: Not set squash file\n"
	printf " # Usage: `basename $0` /path/to/file.sqfs\n" 
	exit 1 
fi

printf " # Run: $COMMAND\n"
$COMMAND

sync
exit 0
