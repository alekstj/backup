#!/bin/bash
#

DATA=$(date '+%Y%d%m')
FILE=stage4-openrc-xfce4-${DATA}.sqfs

$PWD/bin/mksquashfs-x86_64 /mnt/gentoo $PWD/$FILE -not-reproducible -xattrs -wildcards -noappend -progress -mem 2G -comp zstd -e tmp/* var/cache/distfiles/*

md5sum $FILE > ${FILE}.md5

sync
exit 0
